variable "domain_name" {
  type        = string
  description = "The domain name for the website."
  default     = "ramptunes.com"
}

variable "bucket_name" {
  description = "Name of the music songbook website's S3 bucket"
  type        = string
  default     = "bopshebop-website"
}

