output "aws_lambda_function_index_songfiles_id" {
  value = aws_lambda_function.index-songfiles.id
}
output "aws_lambda_function_list_bucket_files_id" {
  value = aws_lambda_function.list-bucket-files.id
}
output "aws_lambda_function_opensearch_lambda_id" {
  value = aws_lambda_function.opensearch-lambda.id
}

output "opensearch_lambda_name" {
  value = aws_lambda_function.opensearch-lambda.function_name
}
output "opensearch_lambda_arn" {
  value = aws_lambda_function.opensearch-lambda.invoke_arn
}
