resource "aws_lambda_layer_version" "tfer--arn-003A-aws-003A-lambda-003A-us-west-1-003A-583087165181-003A-layer-003A-opensearch-py-003A-1" {
  compatible_runtimes = ["python3.7", "python3.8", "python3.9"]
  description         = "Opensearch-py Lib"
  layer_name          = "opensearch-py"
  license_info        = "Bopshebop"
}

resource "aws_lambda_layer_version" "tfer--arn-003A-aws-003A-lambda-003A-us-west-1-003A-583087165181-003A-layer-003A-pandas-003A-1" {
  compatible_runtimes = ["python3.7", "python3.8", "python3.9"]
  description         = "Pandas Lib"
  layer_name          = "pandas"
  license_info        = "Bopshebop"
}

resource "aws_lambda_layer_version" "tfer--arn-003A-aws-003A-lambda-003A-us-west-1-003A-583087165181-003A-layer-003A-requests-003A-1" {
  compatible_runtimes = ["python3.7", "python3.8", "python3.9"]
  description         = "Requests Lib"
  layer_name          = "requests"
  license_info        = "Bopshebop"
}

resource "aws_lambda_layer_version" "tfer--arn-003A-aws-003A-lambda-003A-us-west-1-003A-583087165181-003A-layer-003A-requests_aws4auth-003A-1" {
  compatible_runtimes = ["python3.7", "python3.8", "python3.9"]
  description         = "Requests_aws4auth Lib"
  layer_name          = "requests_aws4auth"
  license_info        = "Bopshebop"
  source_code_hash    = "697vGz1ZDtjAMS4Lx++AhQD972QgINd2OBc+HQHjmDc="
}
