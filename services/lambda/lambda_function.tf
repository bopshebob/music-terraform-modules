resource "aws_lambda_function" "index-songfiles" {
  architectures = ["x86_64"]
  ephemeral_storage {
    size = "512"
  }

  s3_bucket                      = "bopshebop-terraform"
  s3_key                         = "src/lambda/prod/index-songfiles.zip"
  function_name                  = "index-songfiles"
  handler                        = "lambda_function.lambda_handler"
  layers                         = ["arn:aws:lambda:us-west-1:583087165181:layer:opensearch-py:1", "arn:aws:lambda:us-west-1:583087165181:layer:requests_aws4auth:1"]
  memory_size                    = "128"
  package_type                   = "Zip"
  reserved_concurrent_executions = "-1"
  role                           = "arn:aws:iam::583087165181:role/service-role/index-songfiles-role-t65yrjvn"
  runtime                        = "python3.9"
  timeout                        = "300"

  tracing_config {
    mode = "PassThrough"
  }
}

resource "aws_lambda_function" "list-bucket-files" {
  architectures = ["x86_64"]
  ephemeral_storage {
    size = "512"
  }

  s3_bucket                      = "bopshebop-terraform"
  s3_key                         = "src/lambda/prod/list-bucket-files.zip"
  function_name                  = "list-bucket-files"
  handler                        = "lambda_function.lambda_handler"
  memory_size                    = "128"
  package_type                   = "Zip"
  reserved_concurrent_executions = "-1"
  role                           = "arn:aws:iam::583087165181:role/service-role/list-bucket-files-role-cp39v9p8"
  runtime                        = "python3.9"

  tags = {
    Purpose = "code snippet"
  }

  tags_all = {
    Purpose = "code snippet"
  }

  timeout = "300"

  tracing_config {
    mode = "PassThrough"
  }
}

resource "aws_lambda_function" "opensearch-lambda" {
  architectures = ["x86_64"]
  ephemeral_storage {
    size = "512"
  }

  s3_bucket                      = "bopshebop-terraform"
  s3_key                         = "src/lambda/prod/opensearch-lambda.zip"
  function_name                  = "opensearch-lambda"
  handler                        = "lambda_function.lambda_handler"
  layers                         = ["arn:aws:lambda:us-west-1:583087165181:layer:requests:1"]
  memory_size                    = "128"
  package_type                   = "Zip"
  reserved_concurrent_executions = "-1"
  role                           = "arn:aws:iam::583087165181:role/service-role/opensearch-lambda-role-ovn7ngv9"
  runtime                        = "python3.9"
  timeout                        = "300"

  tracing_config {
    mode = "PassThrough"
  }
}
