resource "aws_apigatewayv2_api" "songsearch" {
  name          = "songsearch-http-api"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_stage" "songsearch_stage" {
  api_id      = aws_apigatewayv2_api.songsearch.id
  name        = "$default"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_gw.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
}

resource "aws_apigatewayv2_integration" "songsearch_integration" {
  api_id           = aws_apigatewayv2_api.songsearch.id
  integration_type = "AWS_PROXY"

  description               = "Uses Lambda function to retrieve and enrich OpenSearch result"
  integration_method        = "POST"
  integration_uri           = var.opensearch_lambda_arn
}

resource "aws_apigatewayv2_route" "songsearch_route" {
  api_id    = aws_apigatewayv2_api.songsearch.id
  route_key = "GET /search"

  target = "integrations/${aws_apigatewayv2_integration.songsearch_integration.id}"
}

resource "aws_cloudwatch_log_group" "api_gw" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.songsearch.name}"

  retention_in_days = 7
}

resource "aws_lambda_permission" "songsearch_gw" {
   statement_id  = "AllowAPIGatewaySongsearchInvoke"
   action        = "lambda:InvokeFunction"
   function_name = var.opensearch_lambda_name
   principal     = "apigateway.amazonaws.com"

   # The "/*/*" portion grants access from any method on any resource
   # within the API Gateway REST API.
   source_arn = "${aws_apigatewayv2_api.songsearch.execution_arn}/*/*"
}
