variable "opensearch_lambda_name" {
  type = string
}
variable "opensearch_lambda_arn" {
  type = string
}
