# API Gateway REST API with Lambda

This module builds a REST API with API Gateway, integrated
witha Lambda function. It is based on this AWS tutorial:

https://docs.aws.amazon.com/apigateway/latest/developerguide/getting-started-lambda-non-proxy-integration.html

## Registry modules used

For the API gateway:
https://github.com/SPHTech-Platform/terraform-aws-apigw

For the Lambda function:
https://github.com/terraform-aws-modules/terraform-aws-lambda

