locals {
  name = "bopshebop_api_${var.env}"
  lambda_function_name = "LambdaIntegration-${var.env}"
  lambda_rolename = "APIGatewayLambdaIntegrationRole-${var.env}"
  tags = {
    Env        = var.env
    Name       = local.name
  }
}

# Configure and downloading plugins for aws
provider "aws" {
  region = var.region
}

module "api_gateway" {
  source  = "SPHTech-Platform/apigw/aws"
  version = "0.3.1"
  name = local.name
  stage = "test"
  body_template = file("${path.module}/templates/spec.yml")
  log_retention_in_days = 0
}

module "lambda_function" {
  source = "terraform-aws-modules/lambda/aws"
  version = "4.9.0"

  function_name = local.lambda_function_name
  description   = "Lambda integration for API Gateway"
  handler       = "index.handler"
  runtime       = "nodejs14.x"
  lambda_role   = "arn:aws:iam::${var.aws_account}:role/service-role/${local.lambda_rolename}"
  role_name = local.lambda_rolename
  role_path = "/service-role/"
  policy_path = "/service-role/"
  role_force_detach_policies = false
  publish = true
  allowed_triggers = {
    APIGatewayTest = {
      service    = "apigateway"
      source_arn = "${module.api_gateway.aws_api_gateway_stage_execution_arn}/*/*"
    }
  }
  source_path = "${path.module}/src/index.mjs"

  tags = {
    RoleName   = local.lambda_rolename
    Env        = var.env
    Name       = local.name
  }
}
