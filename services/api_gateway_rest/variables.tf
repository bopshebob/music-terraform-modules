variable "env" {
  type        = string
  description = "The environment for the project (dev, stage, prod...)."
}

variable "region" {
    type        = string
    description = "The region for the project (us-east-1, us-east-2...)."
}

variable "aws_account" {
    type        = string
    description = "The AWS account for the project."
}
