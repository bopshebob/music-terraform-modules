output "aws_api_gateway_rest_api_id" {
  value       = module.api_gateway.aws_api_gateway_rest_api_id
  description = "REST API id of the created api"
}

output "aws_api_gateway_stage_invoke_url" {
  value       = module.api_gateway.aws_api_gateway_stage_invoke_url
  description = "The URL to invoke the API pointing to the stage"
}

