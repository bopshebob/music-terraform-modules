
resource "aws_dynamodb_table" "songs" {
  name         = "songs"
  billing_mode = "PAY_PER_REQUEST"
  read_capacity  = "0"
  write_capacity = "0"
  hash_key     = "S3Location"
  stream_enabled = "false"
  table_class    = "STANDARD"

  attribute {
    name = "S3Location"
    type = "S"
  }

  attribute {
    name = "Title"
    type = "S"
  }

  attribute {
    name = "Artist"
    type = "S"
  }

  attribute {
    name = "Year"
    type = "S"
  }

  attribute {
    name = "Tonality"
    type = "S"
  }

  point_in_time_recovery {
    enabled = "false"
  }

  global_secondary_index {
    name               = "TitleArtistIndex"
    hash_key           = "Title"
    range_key          = "Artist"
    projection_type    = "ALL"
  }

  global_secondary_index {
    name               = "ArtistYearIndex"
    hash_key           = "Artist"
    range_key          = "Year"
    projection_type    = "ALL"
  }

  global_secondary_index {
    name               = "YearIndex"
    hash_key           = "Year"
    projection_type    = "ALL"
  }

  global_secondary_index {
    name               = "TonalityIndex"
    hash_key           = "Tonality"
    projection_type    = "ALL"
  }

  tags = {
    CreatedBy = "Sheldon Rampton"
    ManagedBy = "terraform"
    Name      = "Great American Songbook"
  }

  tags_all = {
    CreatedBy = "Sheldon Rampton"
    ManagedBy = "terraform"
    Name      = "Great American Songbook"
  }

}
