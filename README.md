# music-terraform-modules

Versioned terraform modules for music website

## To release a new version:
```
git tag -a "v0.0.2" -m "New release of modules"
git push --follow-tags
```

## Authors and acknowledgment
Written by Sheldon Rampton.

## License
See files LICENSE.txt and COPYING.